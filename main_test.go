package main

import "testing"

func TestMain(t *testing.T) {
	msg := GetHello()
	if msg != "hello" {
		t.Fatalf(`GetHello() = %q`, msg)
	}
}
