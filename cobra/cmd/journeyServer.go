/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"context"
	"fmt"
	"github.com/spf13/cobra"
	"go_poc/generated/journey"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
	"log"
	"net"
)

type JourneyServer struct {
	journey.UnimplementedJourneyServiceServer
}

func (s *JourneyServer) RelayEvent(ctx context.Context, req *journey.RelayRequest) (*journey.RelayResponse, error) {
	if req == nil {
		fmt.Println("request must not be nil")
		return &journey.RelayResponse{Response: "error"}, xerrors.Errorf("request must not be nil")
	}

	fmt.Println("received " + req.GetUserEvent().UserUuid.GetValue())
	return &journey.RelayResponse{Response: req.GetUserEvent().UserUuid.GetValue()}, nil
}

// journeyServerCmd represents the journeyServer command
var journeyServerCmd = &cobra.Command{
	Use:   "journeyServer",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		lis, err := net.Listen("tcp", journeyPort)
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}

		grpcServer := grpc.NewServer()

		// Register services
		journey.RegisterJourneyServiceServer(grpcServer, &JourneyServer{})

		log.Printf("GRPC server listening on %v", lis.Addr())

		if err := grpcServer.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	},
}

func init() {
	rootCmd.AddCommand(journeyServerCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// journeyServerCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// journeyServerCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
