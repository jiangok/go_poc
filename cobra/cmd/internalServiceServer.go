/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"context"
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	pbc "go_poc/event_logger"
	pb "go_poc/generated"
	"go_poc/generated/internalService"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/wrapperspb"
	"log"
	"net"
)

type InternalServiceServer struct {
	internalService.UnimplementedInternalServiceServer
}

func (s *InternalServiceServer) GetOffer(ctx context.Context, req *internalService.GetOfferRequest) (*internalService.GetOfferResponse, error) {
	if req == nil {
		return &internalService.GetOfferResponse{Response: "error"}, xerrors.Errorf("request must not be nil")
	}

	fmt.Println("received " + req.GetRequest())
	err := pbc.EventLogger{}.CreateAndSendEvent(
		req.GetRequest(),
		pb.UserType_CUSTOMER,
		pb.SourceType_WebsiteSourceType,
		pb.WebsiteSource{Ip: &wrapperspb.StringValue{Value: "10.0.0.1"}, Url: nil, UserAgent: nil},
		pb.SellDirectButtonClickedContext{
			OfferContext: &pb.OfferContext{
				OfferId:     &wrapperspb.Int64Value{Value: 123},
				OfferAmount: &wrapperspb.DoubleValue{Value: 1200},
			},
		},
		nil,
	)

	if err != nil {
		return nil, errors.New("cannot create and send event")
	}

	return &internalService.GetOfferResponse{Response: "success"}, nil
}

// internalServiceServerCmd represents the InternalServiceServer command
var internalServiceServerCmd = &cobra.Command{
	Use:   "internalServiceServer",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		lis, err := net.Listen("tcp", internalServicePort)
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}

		grpcServer := grpc.NewServer(withServerUnaryInterceptor())

		// Register services
		internalService.RegisterInternalServiceServer(grpcServer, &InternalServiceServer{})

		log.Printf("GRPC server listening on %v", lis.Addr())

		if err := grpcServer.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	},
}

func withServerUnaryInterceptor() grpc.ServerOption {
	return grpc.UnaryInterceptor(serverInterceptor)
}

func serverInterceptor(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler) (interface{}, error) {

	getOfferRequest, ok := req.(*internalService.GetOfferRequest)
	if ok {
		fmt.Printf("server interceptor got method: %s, request: %s!\n", info.FullMethod, getOfferRequest.GetRequest())
		err := pbc.EventLogger{}.CreateAndSendEvent(
			getOfferRequest.GetRequest(),
			pb.UserType_CUSTOMER,
			pb.SourceType_WebsiteSourceType,
			pb.WebsiteSource{Ip: &wrapperspb.StringValue{Value: "10.0.0.1"}, Url: nil, UserAgent: nil},
			pb.SellDirectButtonClickedContext{
				OfferContext: &pb.OfferContext{
					OfferId:     &wrapperspb.Int64Value{Value: 123},
					OfferAmount: &wrapperspb.DoubleValue{Value: 1200},
				},
			},
			nil,
		)

		if err != nil {
			return nil, errors.New("cannot create and send event")
		}
	}

	h, err := handler(ctx, req)
	return h, err
}

func init() {
	rootCmd.AddCommand(internalServiceServerCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// internalServiceServerCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// internalServiceServerCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
