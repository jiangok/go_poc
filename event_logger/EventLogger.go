package event_logger

import (
	"context"
	"errors"
	_ "fmt"
	"github.com/google/uuid"
	pb "go_poc/generated"
	_ "go_poc/generated/internalService"
	"go_poc/generated/journey"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/timestamppb"
	"google.golang.org/protobuf/types/known/wrapperspb"
	"log"
	"time"
)

const (
	journeyPort          = ":9000"
	journeyServerAddress = "localhost" + journeyPort
)

type EventLogger struct{}

func (r EventLogger) CreateAndSendEvent(
	userUuid string,
	userType pb.UserType,
	sourceType pb.SourceType,
	websiteSource pb.WebsiteSource,
	ctx pb.SellDirectButtonClickedContext,
	properties map[string]string) error {
	event, err := r.CreateEvent(userUuid, userType, sourceType, websiteSource, ctx, properties)
	if err != nil {
		return errors.New("cannot create event")
	}

	err = r.sendEvent(event)
	if err != nil {
		return errors.New("cannot send event to journey")
	}

	return nil
}

func (r EventLogger) CreateEvent(
	userUuid string,
	userType pb.UserType,
	sourceType pb.SourceType,
	websiteSource pb.WebsiteSource,
	ctx pb.SellDirectButtonClickedContext,
	properties map[string]string) (*pb.UserEvent, error) {

	if _, err := uuid.Parse(userUuid); err != nil {
		return nil, errors.New("userUuid is not an uuid")
	}

	event := pb.UserEvent{
		ContextType:                    pb.UserEventContextType_SellDirectButtonClickedContextType,
		SellDirectButtonClickedContext: &ctx,
		SourceType:                     sourceType,
		WebsiteSource:                  &websiteSource,
		EventCreatedAt:                 timestamppb.Now(),
		UserUuid:                       &wrapperspb.StringValue{Value: userUuid},
		UserType:                       userType,
		Properties:                     properties,
	}

	return &event, nil
}

func (r EventLogger) sendEvent(event *pb.UserEvent) error {
	// create server connection
	var conn *grpc.ClientConn
	conn, err := grpc.Dial(journeyServerAddress, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	_, err = journey.NewJourneyServiceClient(conn).RelayEvent(ctx, &journey.RelayRequest{UserEvent: event})
	if err != nil {
		log.Fatalf("RelayEvent failed: %v", err)
	}

	return nil
}
