/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"context"
	_ "context"
	"fmt"
	"github.com/google/uuid"
	"github.com/spf13/cobra"
	"go_poc/generated/internalService"
	"google.golang.org/grpc"
	_ "google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	_ "google.golang.org/grpc/credentials/insecure"
	"log"
	"time"
	_ "time"
)

const (
	journeyPort                  = ":9000"
	internalServicePort          = ":9001"
	journeyServerAddress         = "localhost" + journeyPort
	internalServiceServerAddress = "localhost" + internalServicePort
)

// internalServiceClientCmd represents the internalServiceClient command
var internalServiceClientCmd = &cobra.Command{
	Use:   "internalServiceClient",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		var conn *grpc.ClientConn
		conn, err := grpc.Dial(internalServiceServerAddress, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			log.Fatalf("did not connect: %s", err)
		}
		defer conn.Close()
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()

		if err != nil {
			log.Fatalf("did not connect: %s", err)
		}

		user_uuid := uuid.NewString()
		fmt.Println("sent " + user_uuid)
		r, err := internalService.NewInternalServiceClient(conn).GetOffer(ctx, &internalService.GetOfferRequest{Request: user_uuid})
		if err != nil {
			log.Fatalf("could not greet: %v", err)
		}
		log.Printf("response: %s", r.GetResponse())
	},
}

func init() {
	rootCmd.AddCommand(internalServiceClientCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// internalServiceClientCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// internalServiceClientCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
