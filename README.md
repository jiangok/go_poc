
# compile grpc service protobuf
```azure
# install compiler
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
# compile server and client.
protoc --go_out=. --go-grpc_out=. $(find schema -iname "*.proto")
```
Note: you need to change `import "./generated` to `import "go_poc/generated"` in 
generated/journey/journey.pb.go, otherwise "go build ./..." will fail.

# build recursively
```azure
cd go_poc
go build ./...
```

# run test recursively
````azure
cd go_poc
go test ./...
````

# set GOPATH
`go_poc/generated` works better than "../generated".
To use the former, you need to set GOPATH correct.
GoLand -> Preference -> Go -> GOPATH, add /Users/lian.jiangopendoor.com/GolandProjects.

# run simulation
In this simulation, internalServiceClient (simulating frontend) sends an uuid to internalServiceServer which
then creates a user event using this uuid and sends it to journeyServer.
```azure
cd cobra
go run main.go journeyServer
go run main.go internalServiceServer
go run main.go internalServiceClient
```
This simulation uses [cobra](https://github.com/spf13/cobra).