This is the cli client for service

# create cli app
```azure
cd cobra
cobra init .
```



# reference
[Learning Go by examples: part 3 - Create a CLI app in Go](https://dev.to/aurelievache/learning-go-by-examples-part-3-create-a-cli-app-in-go-1h43)
[Learning Go by examples: part 6 - Create a gRPC app in Go](https://dev.to/aurelievache/learning-go-by-examples-part-6-create-a-grpc-app-in-go-2ja3)