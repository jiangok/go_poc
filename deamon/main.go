package main

import (
	"fmt"
	"github.com/kardianos/service"
	"time"
)

const serviceName = "Medium deamon"
const serviceDescription = "Simple deamon, just for fun"

type program struct{}

func (p program) Start(s service.Service) error {
	fmt.Println(s.String() + " started")
	go p.run()
	return nil
}

func (p program) run() {
	for {
		fmt.Println("Service is running")
		time.Sleep(3 * time.Second)
	}
}

func (p program) Stop(s service.Service) error {
	fmt.Println(s.String() + " stopped")
	return nil
}

func main() {
	serviceConfig := &service.Config{
		Name:        serviceName,
		DisplayName: serviceName,
		Description: serviceDescription,
	}
	prg := &program{}
	s, err := service.New(prg, serviceConfig)
	if err != nil {
		fmt.Println("Cannot create the deamon: " + err.Error())
	}
	err = s.Run()
	if err != nil {
		fmt.Println("Cannot start the deamon: " + err.Error())
	}
}
