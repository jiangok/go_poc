#!/bin/bash

function build_schema() {
  protoc --go_out=. $(find schema -iname "*.proto")
}