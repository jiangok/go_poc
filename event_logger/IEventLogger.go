package event_logger

import (
	pb "go_poc/generated"
)

type IEventLoggger interface {
	createAndSendEvent(
		userUuid string,
		userType pb.UserType,
		sourceType pb.SourceType,
		websiteSource pb.WebsiteSource,
		ctx pb.SellDirectButtonClickedContext,
		properties map[string]string) error
}
